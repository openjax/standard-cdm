# OpenJAX CDM

> Code Domain Model

[![Build Status](https://travis-ci.org/openjax/cdm.png)](https://travis-ci.org/openjax/cdm)
[![Coverage Status](https://coveralls.io/repos/github/openjax/cdm/badge.svg)](https://coveralls.io/github/openjax/cdm)
[![Javadocs](https://www.javadoc.io/badge/org.openjax/cdm.svg)](https://www.javadoc.io/doc/org.openjax/cdm)
[![Released Version](https://img.shields.io/maven-central/v/org.openjax/cdm.svg)](https://mvnrepository.com/artifact/org.openjax/cdm)

CDM is an early development project to develop a Java API for the expression of the Java language.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

### License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.